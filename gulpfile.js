const assets = require('postcss-assets')({
  basePath: '../../output',
  cache: true,
  cachebuster: true,
  loadPaths: ['images', 'theme/images']
})
const combiner = require('stream-combiner2')
const concat = require('gulp-concat')
const cssDest = 'static/css'
const cssimport = require('postcss-import')
const cssnano = require('cssnano')
const cssnext = require('postcss-cssnext')({
  autoprefixer: {
    browsers: ['last 3 versions']
  },
  warnForDuplicates: false
})
const cssSrc = 'build/css/*.css' // Only work on top-level CSS files; assume others are partials that get imported
const del = require('del')
const fontsDest = 'static/fonts'
const fontsSrc = 'build/fonts/active/**/*.+(woff|woff2|ttf|eot)'
const gulp = require('gulp')
const jsDest = 'static/js'
const jsPath = 'build/js'
const postcss = require('gulp-postcss')
const sorting = require('postcss-sorting')
const sourcemaps = require('gulp-sourcemaps')
const uglify = require('gulp-uglify')

gulp.task('clean:css', function () {
  return del(cssDest)
})

gulp.task('clean:js', function () {
  return del(jsDest)
})

gulp.task('clean:fonts', function () {
  return del(fontsDest)
})

gulp.task('clean', gulp.series(
  'clean:css',
  'clean:js',
  'clean:fonts'
))

gulp.task('build:css', function () {
  const combined = combiner.obj([
    gulp.src(cssSrc),
    sourcemaps.init(),
    postcss([
      cssimport,
      assets,
      cssnext,
      sorting({
        'order': [
          'custom-properties',
          'dollar-variables',
          'declarations',
          'at-rules',
          'rules'
        ],
        'properties-order': [
          'display',
          'flex',
          'flex-flow',
          'align-items',
          'align-content',
          'justify-content',
          'position',
          'bottom',
          'left',
          'right',
          'top',
          'float',
          'clear',
          'height',
          'min-height',
          'max-height',
          'width',
          'min-width',
          'max-width',
          'margin',
          'margin-bottom',
          'margin-left',
          'margin-right',
          'margin-top',
          'padding',
          'padding-bottom',
          'padding-left',
          'padding-right',
          'padding-top',
          'border',
          'border-width',
          'border-bottom-width',
          'border-left-width',
          'border-right-width',
          'border-top-width',
          'border-style',
          'border-bottom-style',
          'border-left-style',
          'border-right-style',
          'border-top-style',
          'border-color',
          'border-bottom-color',
          'border-left-color',
          'border-right-color',
          'border-top-color',
          'border-radius',
          'border-top-left-radius',
          'border-top-right-radius',
          'border-bottom-right-radius',
          'border-bottom-left-radius',
          'opacity',
          'outline',
          'box-shadow',
          'overflow',
          'overflow-x',
          'overflow-y',
          'text-overflow',
          'overflow-wrap',
          'word-break',
          'hyphens',
          'vertical-align',
          'text-align',
          'font-size',
          'line-height',
          'font-family',
          'font-weight',
          'font-style',
          'text-decoration',
          'text-shadow',
          'list-style-type',
          'background',
          'background-image',
          'background-position',
          'background-size',
          'background-repeat',
          'background-origin',
          'background-clip',
          'background-attachment',
          'background-color',
          'color',
          'cursor',
          'transition',
          'transition-delay',
          'transition-duration',
          'transition-property',
          'transition-timing-function'
        ],
        'unspecified-properties-position': 'bottom'
      }),
      cssnano({
        preset: ['default', {
          normalizeWhitespace: false,
          rawCache: false
        }]
      })
    ]),
    sourcemaps.write('.'),
    gulp.dest(cssDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js-bigfoot', function () {
  const combined = combiner.obj([
    gulp.src([`${jsPath}/vendor/bigfoot.js`]),
    sourcemaps.init(),
    uglify(),
    concat('bigfoot.js'),
    sourcemaps.write('./'),
    gulp.dest(jsDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js-fontawesome', function () {
  const combined = combiner.obj([
    gulp.src([
      `${jsPath}/vendor/fontawesome/fontawesome.js`,
      `${jsPath}/vendor/fontawesome/fa-light.js`,
      `${jsPath}/vendor/fontawesome/fa-brands.js`
    ]),
    sourcemaps.init(),
    uglify(),
    concat('fontawesome.js'),
    sourcemaps.write('./'),
    gulp.dest(jsDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js-issocount', function () {
  const combined = combiner.obj([
    gulp.src([`${jsPath}/vendor/isso/commentcount.js`]),
    sourcemaps.init(),
    uglify(),
    concat('isso-commentcount.js'),
    sourcemaps.write('./'),
    gulp.dest(jsDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js-jquery', function () {
  const combined = combiner.obj([
    gulp.src([`${jsPath}/vendor/jquery.js`]),
    sourcemaps.init(),
    uglify(),
    concat('jquery.js'),
    sourcemaps.write('./'),
    gulp.dest(jsDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js-modernizr', function () {
  const combined = combiner.obj([
    gulp.src([`${jsPath}/vendor/modernizr.js`]),
    sourcemaps.init(),
    uglify(),
    concat('modernizr.js'),
    sourcemaps.write('./'),
    gulp.dest(jsDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js-piwik', function () {
  const combined = combiner.obj([
    gulp.src([`${jsPath}/piwik.js`]),
    sourcemaps.init(),
    uglify(),
    concat('piwik.js'),
    sourcemaps.write('./'),
    gulp.dest(jsDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js-tipuesearch_page', function () {
  const combined = combiner.obj([
    gulp.src([`${jsPath}/tipuesearch_page.js`]),
    sourcemaps.init(),
    uglify(),
    concat('tipuesearch_page.js'),
    sourcemaps.write('./'),
    gulp.dest(jsDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js-tipuesearch', function () {
  const combined = combiner.obj([
    gulp.src([`${jsPath}/vendor/tipuesearch/tipuesearch_set.js`, `${jsPath}/vendor/tipuesearch/tipuesearch.js`, `${jsPath}/searchbox.js`]),
    sourcemaps.init(),
    uglify(),
    concat('tipuesearch.js'),
    sourcemaps.write('./'),
    gulp.dest(jsDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js-admonitions', function () {
  const combined = combiner.obj([
    gulp.src([`${jsPath}/admonitions.js`]),
    sourcemaps.init(),
    uglify(),
    concat('admonitions.js'),
    sourcemaps.write('./'),
    gulp.dest(jsDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js-md5', function () {
  const combined = combiner.obj([
    gulp.src([`${jsPath}/vendor/md5.js`]),
    sourcemaps.init(),
    uglify(),
    concat('md5.js'),
    sourcemaps.write('./'),
    gulp.dest(jsDest)
  ])

  combined.on('error', console.error.bind(console))

  return combined
})

gulp.task('build:js', gulp.parallel(
  'build:js-bigfoot',
  'build:js-fontawesome',
  'build:js-issocount',
  'build:js-jquery',
  'build:js-modernizr',
  'build:js-piwik',
  'build:js-tipuesearch_page',
  'build:js-tipuesearch',
  'build:js-admonitions'
))

gulp.task('build:fonts', function () {
  return gulp.src(fontsSrc)
    .pipe(gulp.dest(fontsDest))
})

gulp.task('build', gulp.parallel(
  'build:css',
  'build:js',
  'build:fonts'
))

gulp.task('watch:css', function () {
  gulp.watch('build/css/**/*.css', gulp.series(
    'build:css'
  ))
})

gulp.task('watch:js', function () {
  gulp.watch('build/js/**/*.js', gulp.series(
    'build:js'
  ))
})

gulp.task('watch:fonts', function () {
  gulp.watch(fontsSrc, gulp.series(
    'build:fonts'
  ))
})

gulp.task('watch', gulp.series(
  'build',
  gulp.parallel(
    'watch:css',
    'watch:js',
    'watch:fonts'
  )
))

gulp.task('default',
  gulp.series(
    'clean',
    'watch'
  )
)
