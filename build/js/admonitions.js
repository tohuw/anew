// Admonitions & search warnings are added automatically via Python's Markdown and JavaScript, respectively.
// Rather than hack around that or require macro alternatives, simply add icons to admonition types on page load.

$(document).ready(function () {
  $('#tipue_search_warning').addClass('admonition warning')
  $('.admonition.info').append('<i class="admonition_icon fal fa-3x fa-pull-right fa-info-circle"></i>')
  $('.admonition.note').append('<i class="admonition_icon fal fa-3x fa-pull-right fa-info-circle"></i>')
  $('.admonition.warning').append('<i class="admonition_icon fal fa-3x fa-pull-right fa-exclamation-circle"></i>')
  $('.admonition.caution').append('<i class="admonition_icon fal fa-3x fa-pull-right fa-exclamation-circle"></i>')
  $('.admonition.alert').append('<i class="admonition_icon fal fa-3x fa-pull-right fa-minus-circle"></i>')
  $('.admonition.important').append('<i class="admonition_icon fal fa-3x fa-pull-right fa-minus-circle"></i>')
})
