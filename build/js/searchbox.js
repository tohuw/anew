var placeholder = $('#tipue_search_input').attr('placeholder')

$('#tipue_search_input').focus(function () {
  $('.search').addClass('is-focused')
  $('.search_icon').addClass('is-expanded')
  $(this).attr('placeholder', ' ')
}).blur(function () {
  $(this).val(null)
  $('.search').removeClass('is-focused')
  $(this).attr('placeholder', placeholder)
  setTimeout(function () {
    $('.search_icon').fadeIn(250).removeClass('is-expanded')
  }, 250)
})
