var piwikdata = document.getElementById('piwikscript')
var _paq = _paq || []
_paq.push(['setDomains', [piwikdata.dataset.piwikDomains]])
_paq.push(['trackPageView'])
_paq.push(['enableLinkTracking']);
(function () {
  var u = 'https:' + piwikdata.dataset.piwikUrl
  _paq.push(['setTrackerUrl', u + 'piwik.php'])
  _paq.push(['setSiteId', piwikdata.dataset.piwikSiteid])
  var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0]
  g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'piwik.js'; s.parentNode.insertBefore(g, s)
})()
